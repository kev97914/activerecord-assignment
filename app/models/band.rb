class Band < ActiveRecord::Base
  validates_presence_of :name, :num_members
  validates_numericality_of :num_members, :greater_than => 0

  has_many :bookings
  has_many :clubs, :through => :bookings

end
