class Booking < ActiveRecord::Base
  validates_presence_of :band_id, :club_id, :fee, :date
  validates_numericality_of :fee, :greater_than_or_equal_to => 0

  belongs_to :club
  belongs_to :band

  accepts_nested_attributes_for :club
  accepts_nested_attributes_for :band

end
