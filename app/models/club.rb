class Club < ActiveRecord::Base
  validates_presence_of :name, :street_address

  has_many :bookings
  has_many :bands, :through => :bookings

end
