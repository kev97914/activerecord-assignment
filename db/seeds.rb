# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Band.create(name: '30 Second to Mars', num_members: 4)
Band.create(name: 'Linkin Park', num_members: 6)
Band.create(name: 'One Person Band', num_members: 1)
Band.create(name: 'The Killers' , num_members: 5)

Club.create(name: 'The Concert Show', street_address: '12730 Richmond Ave')
Club.create(name: 'Sherlock Pub', street_address: '15434 Westheimer Road')

Booking.create(band_id: 1, club_id: 1, fee: 50.00, date: '01-03-2014')
Booking.create(band_id: 1, club_id: 2, fee: 75.00, date: '02-03-2014')

Booking.create(band_id: 2, club_id: 1, fee: 40.50, date: '01-04-2014')
Booking.create(band_id: 2, club_id: 2, fee: 50.50, date: '02-04-2014')

Booking.create(band_id: 3, club_id: 1, fee:9.50, date: '04-04-2014')
Booking.create(band_id: 3, club_id: 2, fee:10.00, date: '06-04-2014')

Booking.create(band_id: 4, club_id: 1, fee:35.00, date: '04-08-2014')
Booking.create(band_id: 4, club_id: 2, fee:35.00, date: '06-08-2014')



